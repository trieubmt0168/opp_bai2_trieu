package com.dd.trieu.entity;

public class Product{  private int id;
    private String name;
    private int categoryid;

    private int qulity;
    private boolean isDelete;

    public Product(int id, String name, int categoryid, int qulity, boolean isDelete) {
        this.id = id;
        this.name = name;
        this.categoryid = categoryid;

        this.qulity = qulity;
        this.isDelete = isDelete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }



    public int getQulity() {
        return qulity;
    }

    public void setQulity(int qulity) {
        this.qulity = qulity;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryid=" + categoryid +
                ", qulity=" + qulity +
                ", isDelete=" + isDelete +
                '}';
    }
    public int createProduct(String name, Object id) {
//        int i = 1;
//        if (name.equals("productTable")) {
//            this.getCategoryid();
//            i = 1;
//        }
//

        return 0;
    }

}