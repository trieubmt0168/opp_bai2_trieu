package com.dd.trieu.dao;

import java.util.ArrayList;

public class Database {
    private ArrayList<Object> productTable;
    private ArrayList<Object> categoryTable;
    private ArrayList<Object> accessoryTable;
    private Database instants;


    public Database() {
        this.productTable = new ArrayList<Object>();
        this.categoryTable = new ArrayList<Object>();
        this.accessoryTable = new ArrayList<Object>();

    }


    public ArrayList<Object> getProductTable() {
        return productTable;
    }

    public void setProductTable(ArrayList<Object> productTable) {
        this.productTable = productTable;
    }

    public ArrayList<Object> getCategoryTable() {
        return categoryTable;
    }

    public void setCategoryTable(ArrayList<Object> categoryTable) {
        categoryTable = categoryTable;
    }

    public ArrayList<Object> getAccessoryTable() {
        return accessoryTable;
    }

    public void setAccessoryTable(ArrayList<Object> accessoryTable) {
        this.accessoryTable = accessoryTable;
    }

    public Database getInstants() {
        return instants;
    }

    public void setInstants(Database instants) {
        this.instants = instants;
    }

    @Override
    public String toString() {
        return "Database{" +
                "productTable=" + productTable +
                ", CategoryTable=" + categoryTable +
                ", accessoryTable=" + accessoryTable +
                ", instants=" + instants +
                '}';
    }

    public int insertTable(String name, Object row) {
        int i = 0;
        if (name.equals("productTable")) {
            this.productTable.add(row);
            i = 1;
        }
        if (name.equals("categoryTable")) {
            this.categoryTable.add(row);
            i = 1;
        }
        if (name.equals("accessoryTable")) {
            this.accessoryTable.add(row);
            i = 1;
        }
        return i;
    }

    public ArrayList<Object> selectTable(String name) {

        if (name.equals("product")) {
            return this.productTable;
        }
        if (name.equals("category")) {
            return this.categoryTable;
        }
        if (name.equals("category")) {
            return this.accessoryTable;
        }
        return null;

    }


    //////
//    public int updateTable(String name, Object row) {
//        int i;
//        if (name.equals("productTable1")) {
//            this.productTable.;
//            i = 1;
//        }
//        if (name.equals("categoryTable1")) {
//            this.CategoryTable.add(row);
//            i = 1;
//        }
//        if (name.equals("accessoryTable1")) {
//            this.accessoryTable.add(row);
//            i = 1;
//        }
//        return 1;
//    }
//    ////
//
    public Boolean deleteTable(String name, Object row) {

        if (name.equals("productTable1")) {
            this.productTable.remove(row);

        }
        if (name.equals("categoryTable1")) {
            this.categoryTable.remove(row);

        }
        if (name.equals("accessoryTable1")) {
            this.accessoryTable.remove(row);

        }
        return false;
    }

    public void truncateTable(String name) {
        if (name.equals("productTable1")) {
            this.productTable.clear();
            System.out.println("TRuncateProduct");

        }
        if (name.equals("categoryTable1")) {
            this.categoryTable.clear();
            System.out.println("TruncateCategory");

        }
        if (name.equals("accessoryTable1")) {
            this.categoryTable.clear();
            System.out.println("TruncateAccesory");

        }

    }
}
