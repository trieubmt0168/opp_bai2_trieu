package com.dd.trieu.demo;


import com.dd.trieu.dao.Database;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;

public class ProductDemo {
    static Database database = new Database();


    public static void createProductTest() {

        ArrayList<Product> productList = new ArrayList<Product>();
        productList.add(new Product(1, "thanh Long", 1, 1, false));
        productList.add(new Product(2, "Thơm", 2, 1, true));
        productList.add(new Product(3, "Mì Tôm", 2, 6, true));
        productList.add(new Product(4, "Bưởi", 3, 4, true));
        productList.add(new Product(5, "Ổi", 3, 0, true));
        productList.add(new Product(6, "Mận", 2, 0, false));
        productList.add(new Product(7, "táo", 2, 0, false));
        productList.add(new Product(8, "Nho", 1, 0, false));
        productList.add(new Product(9, "Cam", 1, 0, true));
        productList.add(new Product(10, "Mướp", 1, 1, false));
        database.insertTable("productList", productList);
        System.out.println(productList);

    }
private static void printProduct(){

    System.out.println("in");
    System.out.println(new Product(1, "thanh Long", 1, 1, false));
    }

    public static void main(String[] args) {
        createProductTest();
        printProduct();

    }

}
