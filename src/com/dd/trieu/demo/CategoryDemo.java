package com.dd.trieu.demo;

import com.dd.trieu.dao.Database;
import com.dd.trieu.entity.Category;

import java.util.ArrayList;

public class CategoryDemo {
    static Database database = new Database();

    public static void createCategoryTest() {

        ArrayList<Category> categories = new ArrayList<Category>();
        categories.add(new Category(1, "Đồ ăn"));
        categories.add(new Category(2, "Đồ ăn"));
        categories.add(new Category(3, "Đồ uống"));
        categories.add(new Category(4, "Đồ uống"));
        categories.add(new Category(5, "Đồ uống"));
        categories.add(new Category(6, "Đồ uống"));
        categories.add(new Category(7, "Đồ uống"));
        categories.add(new Category(8, "Đồ uống"));
        categories.add(new Category(9, "Đồ uống"));
        categories.add(new Category(10, "Đồ uống"));
        System.out.println(categories);
    }
    private static void printCategory(){

        System.out.println("in");
        System.out.println(new Category(10, "Đồ uống"));
    }

    public static void main(String[] args) {

        createCategoryTest();
        printCategory();
    }
}
